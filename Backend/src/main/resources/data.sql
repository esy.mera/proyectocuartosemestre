INSERT INTO authorities (name, endpoint) values ('User_Write', 'POST:/api/user/');
INSERT INTO authorities (name, endpoint) values ('User_Read', 'GET:/api/user/');
INSERT INTO authorities (name, endpoint) values ('UserById_Read', 'GET:/api/user/{id}/');
INSERT INTO authorities (name, endpoint) values ('User_Delete', 'DELETE:/api/user/{id}/');
INSERT INTO authorities (name, endpoint) values ('User_Update', 'PUT:/api/user/{id}/');
INSERT INTO authorities (name, endpoint) values ('User_PartialUpdate', 'PATCH:/api/user/{id}/');

INSERT INTO authorities (name, endpoint) values ('Role_Write', 'POST:/api/role/');
INSERT INTO authorities (name, endpoint) values ('Role_Read', 'GET:/api/role/');
INSERT INTO authorities (name, endpoint) values ('RoleById_Read', 'GET:/api/role/{id}/');
INSERT INTO authorities (name, endpoint) values ('Role_Delete', 'DELETE:/api/role/{id}/');
INSERT INTO authorities (name, endpoint) values ('Role_Update', 'PUT:/api/role/{id}/');
INSERT INTO authorities (name, endpoint) values ('Role_PartialUpdate', 'PATCH:/api/role/');

INSERT INTO authorities (name, endpoint) values ('Authority_Write', 'POST:/api/authority/');
INSERT INTO authorities (name, endpoint) values ('Authority_Read', 'GET:/api/authority/');
INSERT INTO authorities (name, endpoint) values ('AuthorityById_Read', 'GET:/api/authority/{id}/');
INSERT INTO authorities (name, endpoint) values ('Authority_Delete', 'DELETE:/api/authority/{id}/');
INSERT INTO authorities (name, endpoint) values ('Authority_Update', 'PUT:/api/authority/{id}/');
INSERT INTO authorities (name, endpoint) values ('Authority_PartialUpdate', 'PATCH:/api/authority/{id}/');
INSERT INTO authorities (name, endpoint) values ('AuthorityByUser_Read', 'GET:/api/authority/byuser/{id}/');

/*INSERT INTO authorities (name, endpoint) values ('Customer_Write', 'POST:/api/cliente/');
INSERT INTO authorities (name, endpoint) values ('Customer_Read', 'GET:/api/cliente/');
INSERT INTO authorities (name, endpoint) values ('CustomerById_Read', 'GET:/api/cliente/{id}/');
INSERT INTO authorities (name, endpoint) values ('Customer_Delete', 'DELETE:/api/cliente/{id}');
INSERT INTO authorities (name, endpoint) values ('Customer_Update', 'PUT:/api/cliente/{id}');
INSERT INTO authorities (name, endpoint) values ('Customer_PartialUpdate', 'PATCH:/api/cliente/{id}');

INSERT INTO authorities (name, endpoint) values ('Product_Write', 'POST:/api/product/');
INSERT INTO authorities (name, endpoint) values ('Product_Read', 'GET:/api/product/');
INSERT INTO authorities (name, endpoint) values ('ProductById_Read', 'GET:/api/product/{id}/');
INSERT INTO authorities (name, endpoint) values ('Product_Delete', 'DELETE:/api/product/{id}/');
INSERT INTO authorities (name, endpoint) values ('Product_Update', 'PUT:/api/product/{id}/');
INSERT INTO authorities (name, endpoint) values ('Product_PartialUpdate', 'PATCH:/api/product/{id}/');

INSERT INTO authorities (name, endpoint) values ('Invoice_Write', 'POST:/api/factura/');
INSERT INTO authorities (name, endpoint) values ('Invoice_Read', 'GET:/api/factura/');
INSERT INTO authorities (name, endpoint) values ('InvoiceById_Read', 'GET:/api/factura/{id}/');
INSERT INTO authorities (name, endpoint) values ('Invoice_Delete', 'DELETE:/api/factura/{id}/');
INSERT INTO authorities (name, endpoint) values ('Invoice_Update', 'PUT:/api/factura/{id}/');
INSERT INTO authorities (name, endpoint) values ('Invoice_PartialUpdate', 'PATCH:/api/factura/{id}/');
INSERT INTO authorities (name, endpoint) values ('InvoicePdf_Read', 'GET:/api/factura/pdf/{id}/');*/

/*datos de autorizacion*/
INSERT INTO authorities (name, endpoint) values ('clientes-getOne', 'clientes-getOne'),('clientes-save', 'clientes-save'),('clientes-update', 'clientes-update'),('clientes-put', 'clientes-put'),('clientes-patch', 'clientes-patch'),('clientes-delete', 'clientes-delete');

INSERT INTO authorities (name, endpoint) values ('proveedores-getAll', 'proveedores-getAll'),('proveedores-getOne', 'proveedores-getOne'),('proveedores-save', 'proveedores-save'),('proveedores-update', 'proveedores-update'),('proveedores-put', 'proveedores-put'),('proveedores-patch', 'proveedores-patch'),('proveedores-delete', 'proveedores-delete');

INSERT INTO authorities (name, endpoint) values ('ciudad-getAll', 'ciudad-getAll'),('ciudad-getOne', 'ciudad-getOne'),('ciudad-save', 'ciudad-save'),('ciudad-update', 'ciudad-update'),('ciudad-put', 'ciudad-put'),('ciudad-patch', 'ciudad-patch'),('ciudad-delete', 'ciudad-delete');

INSERT INTO authorities (name, endpoint) values ('categoria-getOne', 'categoria-getOne'),('categorias-getAll', 'categorias-getAll'),('categoria-save', 'categoria-save'),('categoria-update', 'categoria-update'),
('categoria-delete', 'categoria-delete');

INSERT INTO authorities (name, endpoint) values ('almacen-getAll', 'almacen-getAll'),('almacen-getOne', 'almacen-getOne'),('almacen-save', 'almacen-save'),('almacen-update', 'almacen-update'),('almacen-delete', 'almacen-delete');

INSERT INTO authorities (name, endpoint) values ('compania-getAll', 'compania-getAll'),('compania-getOne', 'compania-getOne'),('compania-save', 'compania-save'),('compania-put', 'compania-put'),('acompania-delete', 'compania-delete');

INSERT INTO authorities (name, endpoint) values ('producto-getOne', 'producto-getOne'),('productos-getAll', 'productos-getAll'),('productos-save', 'productos-save'),('productos-update', 'productos-update'),('productos-delete', 'productos-delete');

INSERT INTO roles (name) values ('ROLE_ADMIN');
/*INSERT INTO roles (name) values ('ROLE_ASESOR');
INSERT INTO roles (name) values ('ROLE_FACTURADOR');*/

INSERT INTO roles_authorities (role_id, authority_id) (select (SELECT id FROM roles where name = 'ROLE_ADMIN')  AS role_id, e.id from authorities e );

/*datos compania*/
INSERT INTO compania (nombre_compania, direccion_compania, telefono_compania, correo_compania)
VALUES
  ('Compañía A', 'Dirección A', 123456789, 'correo1@example.com'),
  ('Compañía B', 'Dirección B', 987654321, 'correo2@example.com');

INSERT INTO users (name, username, password, looked, expired, enabled,compania_id) VALUES ('Admin', 'admin', '$2a$10$TwROhi2MZsOTt8igkE7Yyec0WfjK7NlgdX9apOu0b6cY4SxzHLvCq', false, false, true,1);

INSERT INTO users_roles (user_id, role_id) VALUES ((SELECT id FROM users where username = 'admin'), (SELECT id FROM roles where name = 'ROLE_ADMIN'));

/*datos Ciudad*/

INSERT INTO ciudad (nombre) values ('QUITO'),('GUAYAQUIL'),('CUENCA'),('AMBATO'),('MANTA'),('MACHALA'),('LOJA'),('IBARRA');


/*datos categorias*/
INSERT INTO categorias (nombre, descripcion)
VALUES
 ('Sandalias', 'Calzado abierto y ligero para climas cálidos'),
 ('Botas', 'Zapatos con caña alta para protección y estilo en climas fríos');



  
/*datos usuario*/
INSERT INTO users (password, name, username, looked, expired, enabled, compania_id)
VALUES
  ('password1', 'Nombre1', 'username1', false, false,  true,1),
  ('password2', 'Nombre2', 'username2', false, false, true,2),
  ('password3', 'Nombre3', 'username3', false, false, true,1),
  ('password4', 'Nombre4', 'username4', false, false, true,2),
  ('password5', 'Nombre5', 'username5', false, false, true,1);


/*datos almacen*/
INSERT INTO almacen (nombre_almacen, direccion_almacen, capacidad_almacenamiento, id_ciudad_id, id_compania_id)
VALUES
  ('Almacén A', 'Dirección A', 100, 1, 1),
  ('Almacén B', 'Dirección B', 200, 2, 2),
  ('Almacén C', 'Dirección C', 150, 3, 1),
  ('Almacén D', 'Dirección D', 300, 4, 2),
  ('Almacén E', 'Dirección E', 250, 5, 1);
/*datos proveedores*/
INSERT INTO proveedores (nombre, direccion, id_ciudad, id_almacen)
VALUES
  ('Proveedor A', 'Dirección A',1,1),
  ('Proveedor B', 'Dirección B',2,2),
  ('Proveedor C', 'Dirección C',3,3),
  ('Proveedor D', 'Dirección D',4,4),
  ('Proveedor E', 'Dirección E', 5,5);
/*datos productos*/
/*INSERT INTO productos (nombre, descripcion, precio_unitario, stock_minimo, cantidad, id_categoria_id, proveedores_id_proveedor)
VALUES
  ('Zapatos deportivos', 'Zapatos deportivos para correr, color negro', 89.99, 25, 10,1,1),
  ('Zapatos de vestir', 'Zapatos de vestir de cuero, color marrón', 129.99, 15, 5,2,2),
  ('Zapatos casuales', 'Zapatos casuales de tela, color azul', 59.99, 30, 8,1,3),
  ('Zapatillas deportivas', 'Zapatillas deportivas para gimnasio, color gris', 69.99, 20, 7,2,4),
  ('Zapatos elegantes', 'Zapatos elegantes de cuero, color negro', 149.99, 10, 3,1,5);*/




