package erp.pedidos.proveedores;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class proveedoresService {

    @Autowired
    private proveedoresRepository base;

    //mostrar todos
    public ArrayList<proveedores> getAll(){
        return (ArrayList<proveedores>) this.base.findAll();
    }

    //mostrar uno
    public Optional<proveedores> getOne(Long id){
        return this.base.findById(id);
    }

    //guardar nuevo
    public proveedores save(proveedores persona){
        return this.base.save(persona);
    }

    //actualizar todo
    public proveedores put(Long id, proveedores personaAc){
        proveedores datos = this.base.findById(id).get();
        datos.setNombre(personaAc.getNombre());
        datos.setDireccion(personaAc.getDireccion());
        this.base.save(datos);
        return datos;
    }

    //actualizar por campos
    public proveedores patch(Long id, proveedores personaAc){
        proveedores datos = this.base.findById(id).get();
        datos.setNombre((personaAc.getNombre() != null) ? personaAc.getNombre() : datos.getNombre());
        datos.setDireccion((personaAc.getDireccion() != null) ? personaAc.getDireccion() : datos.getDireccion());
        datos.setCiudad((personaAc.getCiudad() != null) ? personaAc.getCiudad() : datos.getCiudad()); 
        this.base.save(datos);
        return datos;
    }

    //eliminar
    public String delete(Long id){
        this.base.deleteById(id);
        return "eliminado";
    }

    public List proveedoresEmpresa(Long id,long pagina){
        return (List) this.base.proveedoresPorCompania(id, pagina);
    }

    public List proveedoresLink(Long id){
        return (List) this.base.proveedoresLinks(id);
    }
}
