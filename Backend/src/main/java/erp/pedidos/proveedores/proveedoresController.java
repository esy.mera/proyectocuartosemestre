package erp.pedidos.proveedores;

    import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;

//import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/proveedores")
public class proveedoresController {

    @Autowired
    public proveedoresService service;

    //obtener todos
    @Operation(summary = "Obtiene todas los proveedores, Requiere proveedores-getAll")
    @PreAuthorize("hasAuthority('proveedores-getAll')")
    @GetMapping("/")
    public List<proveedores> getAll(){ //List o ArrayList es lo mismo
        return this.service.getAll();
    }

    //obtener uno
    @Operation(summary = "Obtiene un proveedor por su id, Requiere proveedores-getOne")
    @PreAuthorize("hasAuthority('proveedores-getOne')")
    @GetMapping("/{id}/")
    public Optional<proveedores> getOne(@PathVariable("id") Long id){
        return this.service.getOne(id);
    }

    //agregar
    @Operation(summary = "Agrega un proveedor, Requiere proveedores-save")
    @PreAuthorize("hasAuthority('proveedores-save')")
    @PostMapping("/")
    public proveedores save(@RequestBody proveedores body){ //post
        return this.service.save(body);
    }

    //actualizar campo completo el id va en el body
    @Operation(summary = "Actualizar campo completo de un proveedor, el id va en el body , Requiere proveedores-update")
    @PreAuthorize("hasAuthority('proveedores-update')")
    @PutMapping("/")
    public proveedores update(@RequestBody proveedores entity){
        return this.service.save(entity);
    }

    //actualizar campo completo el id va en la url
    @Operation(summary = "Actualizar campo completo de un proveedor, el id va en la url, Requiere proveedores-put")
    @PreAuthorize("hasAuthority('proveedores-put')")
    @PutMapping("/{id}/")
    public proveedores put( @RequestBody proveedores per, @PathVariable("id") Long id){
        return this.service.put(id,per);
    }

    //actualizar por campos
    @Operation(summary = "Actualizar un proveedor por campos, el id va en la url, Requiere proveedores-patch")
    @PreAuthorize("hasAuthority('proveedores-patch')")
    @PatchMapping("/{id}/")
    public proveedores patch( @RequestBody proveedores per, @PathVariable("id") Long id){
        return this.service.patch(id,per);
    }

    //eliminar
    @Operation(summary = "Elimina un proveedor, el id va en la url, Requiere proveedores-delete")
    @PreAuthorize("hasAuthority('proveedores-delete')")
    @DeleteMapping("/{id}/")
    public String delete(@PathVariable("id") Long id){
        return this.service.delete(id);
    }

    @GetMapping("/empresa/{id}/{pagina}")
    public List proveedoresEmpresa(@PathVariable("id") Long id, @PathVariable("pagina") Long pagina){
        return this.service.proveedoresEmpresa(id,pagina);
    }

    @GetMapping("/link/{id}/")
    public List proveedoresLink(@PathVariable("id") Long id){
        return this.service.proveedoresLink(id);
    }

}

// al usar @RequestParam se auto configura sea string o integer
    /*@GetMapping("param")
    public String para(@RequestParam("nombre") String nombre){
        return nombre + 1;
        //http://localhost:3000/param?nombre=sebas  imprime sebas
    }

    @GetMapping("param2")
    public Integer para(@RequestParam("num") Integer num){
        return num + 1;
        //http://localhost:3000/param2?num=2  imprime 3
    }*/
