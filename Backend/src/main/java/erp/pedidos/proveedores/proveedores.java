package erp.pedidos.proveedores;


import erp.pedidos.almacen.almacen;
import erp.pedidos.ciudad.ciudad;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
//@Table(name = "proveedores") //(opcional)
public class proveedores{ // en la base debe estar el mismo nombre pero en minuscula

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProveedor;
    private String nombre; 
    private String direccion;

    
    @ManyToOne
    @JoinColumn(name = "id_ciudad")
    private ciudad ciudad;

    @ManyToOne
    @JoinColumn(name = "id_almacen")
    private almacen idAlmacen;
}
