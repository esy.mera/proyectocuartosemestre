package erp.pedidos.proveedores;

    import java.util.List;

import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


//tambien se puede usar JpaRepository es mejor por unas funciones extras pero trabajan igual
public interface proveedoresRepository extends CrudRepository<proveedores, Long> {
    //Persona es el objeto clase que se aplicara los metodos
    //Long es el atributo que quiere que se base que es el ID de la Entidad persona
    @Query(value = "SELECT pro.id_proveedor,pro.direccion,pro.nombre, ci.nombre, al.nombre_almacen, ci.id, al.id FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN proveedores pro ON pro.id_almacen = al.id INNER JOIN ciudad ci ON ci.id = pro.id_ciudad WHERE com.id = :compania ORDER BY pro.id_proveedor LIMIT 5 OFFSET ((:pagina * 5) - 5);", nativeQuery = true)
    List proveedoresPorCompania(@Param("compania") Long compania, @Param("pagina") Long pagina);

    @Query(value = "SELECT pro.id_proveedor,pro.direccion,pro.nombre, ci.nombre, al.nombre_almacen, ci.id, al.id FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN proveedores pro ON pro.id_almacen = al.id INNER JOIN ciudad ci ON ci.id = pro.id_ciudad WHERE com.id = :compania ORDER BY pro.id_proveedor;", nativeQuery = true)
    List proveedoresLinks(@Param("compania") Long compania);
}
