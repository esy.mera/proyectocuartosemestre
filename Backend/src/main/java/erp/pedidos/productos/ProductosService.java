package erp.pedidos.productos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductosService {
    
    @Autowired
    ProductosRepository repository;
    //CRUD
    //Delimitador de acceso (public, private) tipo de dato de retorno, nombre del metodo, param,etros de entrada [sentencias]
    public Productos save(Productos entity){
        return repository.save(entity);
    }

    public void deleteById(long id){
        repository.deleteById(id);
    }

    public Productos findById(long id){
        return repository.findById(id).orElse(null);
    }

    public List<Productos> findAll(){
        return repository.findAll();
    }

    public List porEmpresa(long id, long pagina){
        return repository.findProductos(id, pagina);
    }

    public List notificacion(long empresa){
        return repository.notificaciones(empresa);
    }
}
