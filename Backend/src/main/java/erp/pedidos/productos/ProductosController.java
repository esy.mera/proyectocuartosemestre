package erp.pedidos.productos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;

@RestController
//Es una ruta de acceso para manejar solicitudes http
@RequestMapping("/api/producto")
//Nos permite controlar solicitudes de recursos crusados
@CrossOrigin({"*"})
public class ProductosController {
    
    @Autowired
    ProductosService service;

    @Operation(summary = "Obtiene un producto por su id, Requiere producto-getOne")
    @PreAuthorize("hasAuthority('producto-getOne')")
    @GetMapping("/{id}/")
    public Productos findById(@PathVariable long id){
       return service.findById(id);
    }

    @Operation(summary = "Obtiene todas los productos, Requiere productos-getAll")
    @PreAuthorize("hasAuthority('productos-getAll')")
    @GetMapping("/")
    public List<Productos> findAll(){
        return service.findAll();
    }

    //Create
    @Operation(summary = "Agrega un producto, Requiere productos-save")
    @PreAuthorize("hasAuthority('productos-save')")
    @PostMapping("/")
    public Productos save (@RequestBody Productos entity){
       return service.save(entity);
    }

    @Operation(summary = "Actualizar campo completo de un producto, el id va en el body , Requiere productos-update")
    @PreAuthorize("hasAuthority('productos-update')")
    @PutMapping("/")
    public Productos update (@RequestBody Productos entity){
        return service.save(entity);
    }

    @Operation(summary = "Elimina un producto, el id va en la url, Requiere productos-delete")
    @PreAuthorize("hasAuthority('productos-delete')")
    @DeleteMapping("/{id}/")
    public void deeteById(@PathVariable long id){
        service.deleteById(id);
    }

    @GetMapping("/PorEmpresa/{id}/")
    public List porEmpresa(@PathVariable("id") long id, @RequestParam("pagina") Integer pagina){
        return service.porEmpresa(id, pagina);
    }

    @GetMapping("/notificaciones/{id}/")
    public List notificaciones(@PathVariable("id") long id){
        return service.notificacion(id);
    }

}
