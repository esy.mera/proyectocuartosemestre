package erp.pedidos.productos;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProductosRepository extends CrudRepository <Productos, Long> {
        List<Productos> findAll();
        @Query(value = "SELECT produ.id,produ.nombre, produ.descripcion, produ.stock_minimo, produ.precio_unitario, produ.cantidad,pro.nombre as proveedor, cate.nombre as categoria, produ.image_id, cate.id, pro.id_proveedor FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN proveedores pro ON pro.id_almacen = al.id INNER JOIN productos produ ON pro.id_proveedor= produ.proveedores_id_proveedor INNER JOIN categorias cate ON cate.id = produ.id_categoria_id WHERE com.id = :compania ORDER BY produ.cantidad LIMIT 5 OFFSET ((:pagina * 5) - 5);", nativeQuery = true)
        List findProductos(@Param("compania") Long compania, @Param("pagina") Long pagina);

        @Query(value = "SELECT produ.id, produ.nombre, produ.stock_minimo, produ.cantidad FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN proveedores pro ON pro.id_almacen = al.id INNER JOIN productos produ ON pro.id_proveedor= produ.proveedores_id_proveedor WHERE com.id = :compania And produ.cantidad <= produ.stock_minimo;", nativeQuery = true)
        List notificaciones(@Param("compania") Long compania);
}
