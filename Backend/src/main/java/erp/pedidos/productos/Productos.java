package erp.pedidos.productos;

import java.math.BigDecimal;
import erp.pedidos.categorias.Categorias;
import erp.pedidos.imagenesProductos.Image;
import erp.pedidos.proveedores.proveedores;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;
@Data
@Entity
public class Productos {
    //Delimitador de acceso, tipo de dato, nombre del atributo, cierre con punto y como;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nombre;
    private String descripcion;
    private BigDecimal precioUnitario;
    private Integer stockMinimo;
    private Integer cantidad;

    @ManyToOne
    private Categorias idCategoria;

    @ManyToOne
    private proveedores Proveedores;

    @ManyToOne
    private Image Image;
} 
