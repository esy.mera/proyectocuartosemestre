package erp.pedidos.imagenesProductos;


import org.springframework.data.jpa.repository.JpaRepository;


public interface ImageRepository extends JpaRepository<Image, Long> {
    /*@Query(value = "SELECT * FROM images WHERE category = :category", nativeQuery = true)
    List<Image> findByCategory(@Param("category") String category);*/
}
