package erp.pedidos.imagenesProductos;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/images")
@CrossOrigin("*")
public class ImageController {
    @Autowired
    private ImageRepository imageRepository;

    @PostMapping
    public Image subirImagen(@RequestParam("file") MultipartFile file) throws IOException {
        Image image = new Image();
        image.setNombre(file.getOriginalFilename());
        image.setTipo(file.getContentType());
        image.setDatos(file.getBytes());

        return imageRepository.save(image);
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> obtenerImagen(@PathVariable("id") Long id) {

        Optional<Image> optionalImage = imageRepository.findById(id);

        if (optionalImage.isPresent()) {
            Image image = optionalImage.get();
            return ResponseEntity.ok().contentType(MediaType.parseMediaType(image.getTipo())).body(image.getDatos());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Image>> listarImagenes() {
        List<Image> images = imageRepository.findAll();
        return ResponseEntity.ok().body(images);
    }
    ///2/

    @GetMapping("/{id}/")
    public ResponseEntity<Optional<Image>> imagen(@PathVariable("id") Long id) {
        Optional<Image> images = imageRepository.findById(id);
        return ResponseEntity.ok().body(images);
    }

    @DeleteMapping("/{id}/")
    public String EliminarImage(@PathVariable("id") Long id) {
        imageRepository.deleteById(id);
        return "images";
    }
}