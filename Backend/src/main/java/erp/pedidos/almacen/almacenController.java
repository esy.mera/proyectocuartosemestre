package erp.pedidos.almacen;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/almacen")
@CrossOrigin({"*"})
public class almacenController {
    
    @Autowired
    almacenService service;

    @Operation(summary = "Obtiene todas los almacenes, Requiere almacen-getAll")
    @PreAuthorize("hasAuthority('almacen-getAll')")
    @GetMapping("/")
    public List<almacen> getAll(){ //List o ArrayList es lo mismo
        return service.getAll();
    }

    @Operation(summary = "Obtiene un almacen por su id, Requiere almacen-getOne")
    @PreAuthorize("hasAuthority('almacen-getOne')")
    @GetMapping("/{id}/")
    public almacen findById(@PathVariable long id){
       return service.findById(id);
    }

    @Operation(summary = "Agrega una almacen, Requiere almacen-save")
    @PreAuthorize("hasAuthority('almacen-save')")
    @PostMapping("/")
    public almacen save (@RequestBody almacen entity){
       return service.save(entity);
    }

    @Operation(summary = "Actualizar campo completo de una almacen, el id va en el body , Requiere almacen-update")
    @PreAuthorize("hasAuthority('almacen-update')")
    @PutMapping("/")
    public almacen update (@RequestBody almacen entity){
        return service.save(entity);
    }

    @Operation(summary = "Elimina una ciudad, el id va en la url, Requiere almacen-delete")
    @PreAuthorize("hasAuthority('almacen-delete')")
    @DeleteMapping("/{id}/")
    public void deeteById(@PathVariable long id){
        service.deleteById(id);
    }

    @GetMapping("/empresa/{id}/{pagina}")
    public List proveedoresEmpresa(@PathVariable("id") Long id, @PathVariable("pagina") Long pagina){
        return service.almacenEmpresa(id,pagina);
    }

    @GetMapping("/empresa/{id}/")
    public List proveedoresLink(@PathVariable("id") Long id){
        return service.almacenesLink(id);
    }

}
