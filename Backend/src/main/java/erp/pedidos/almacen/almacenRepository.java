package erp.pedidos.almacen;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface almacenRepository extends CrudRepository <almacen, Long> {
    @Query(value = "SELECT al.id,al.capacidad_almacenamiento, al.nombre_almacen,al.direccion_almacen,ci.nombre, ci.id FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN ciudad ci ON ci.id = al.id_ciudad_id WHERE com.id = :compania ORDER BY al.id LIMIT 5 OFFSET ((:pagina * 5) - 5);", nativeQuery = true)
    List almacenesPorCompania(@Param("compania") Long compania, @Param("pagina") Long pagina);

    @Query(value = "SELECT al.id,al.capacidad_almacenamiento, al.nombre_almacen,al.direccion_almacen,ci.nombre, ci.id FROM compania com INNER JOIN almacen al ON al.id_compania_id=com.id INNER JOIN ciudad ci ON ci.id = al.id_ciudad_id WHERE com.id = :compania ORDER BY al.id;", nativeQuery = true)
    List almacenesLink(@Param("compania") Long compania);
}
