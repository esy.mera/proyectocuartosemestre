package erp.pedidos.almacen;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class almacenService {

    @Autowired
    almacenRepository repository;

    public ArrayList<almacen> getAll() {
        return (ArrayList<almacen>) this.repository.findAll();
    }

    public almacen save(almacen entity) {
        return repository.save(entity);
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public almacen findById(long id) {
        return repository.findById(id).orElse(null);
    }

     public List almacenEmpresa(Long idCompania, Long pagina){
        return (List) this.repository.almacenesPorCompania(idCompania, pagina);
    }

     public List almacenesLink(Long idCompania){
        return (List) this.repository.almacenesLink(idCompania);
    }
}
