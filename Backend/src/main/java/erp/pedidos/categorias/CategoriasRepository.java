package erp.pedidos.categorias;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CategoriasRepository extends CrudRepository <Categorias, Long> {
    List<Categorias> findAll();

    @Query(value = "SELECT * FROM categorias cate ORDER BY cate.id LIMIT 5 OFFSET ((:pagina * 5) - 5)", nativeQuery = true)
    List categoriasAll(@Param("pagina") Long pagina);
}
