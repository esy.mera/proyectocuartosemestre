package erp.pedidos.categorias;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;

@RestController
// Es una ruta de acceso para manejar solicitudes http
@RequestMapping("/api/categorias")
// Nos permite controlar solicitudes de recursos crusados
@CrossOrigin({ "*" })
public class CategoriasController {

    @Autowired
    CategoriasService service;

    @Operation(summary = "Obtiene una categoria por su id, Requiere categoria-getOne")
    @PreAuthorize("hasAuthority('categoria-getOne')")
    @GetMapping("/{id}/")
    public Categorias findById(@PathVariable long id) {
        return service.findById(id);
    }

    @Operation(summary = "Obtiene todas las categorias, Requiere categorias-getAll")
    @PreAuthorize("hasAuthority('categorias-getAll')")
    @GetMapping("/")
    public List<Categorias> findAll() {
        return service.findAll();
    }

    // Create
    @Operation(summary = "Agrega una categoria, Requiere categoria-save")
    //@PreAuthorize("hasAuthority('categoria-save')")
    @PostMapping("/")
    public Categorias save(@RequestBody Categorias entity) {
        return service.save(entity);
    }

    @Operation(summary = "Actualizar campo completo de una categoria, el id va en el body , Requiere categoria-update")
    @PreAuthorize("hasAuthority('categoria-update')")
    @PutMapping("/")
    public Categorias update(@RequestBody Categorias entity) {
        return service.save(entity);
    }

    @Operation(summary = "Elimina una categoria, el id va en la url, Requiere categoria-delete")
    @PreAuthorize("hasAuthority('categoria-delete')")
    @DeleteMapping("/{id}/")
    public void deeteById(@PathVariable long id) {
        service.deleteById(id);
    }

    @GetMapping("/categorias/{id}/")
    public List categoria(@PathVariable("id") long id) {
        return service.categorias(id);
    }

}