package erp.pedidos.ciudad;

    import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;


@Service
public class ciudadService {

    @Autowired
    private ciudadRepository base;

    //mostrar todos
    public ArrayList<ciudad> getAll(){
        return (ArrayList<ciudad>) this.base.findAll();
    }

    //mostrar uno
    public Optional<ciudad> getOne(Long id){
        return this.base.findById(id);
    }

    //guardar nuevo
    public ciudad save(ciudad persona){
        return this.base.save(persona);
    }

    //actualizar todo
    public ciudad put(Long id, ciudad personaAc){
        ciudad datos = this.base.findById(id).get();
        datos.setNombre(personaAc.getNombre());
        this.base.save(datos);
        return datos;
    }

    //actualizar por campos
    public ciudad patch(Long id, ciudad personaAc){
        ciudad datos = this.base.findById(id).get();
        datos.setNombre((personaAc.getNombre() != null) ? personaAc.getNombre() : datos.getNombre());
        //datos.setProveedores((personaAc.getProveedores() != null) ? personaAc.getProveedores() : datos.getProveedores());
        this.base.save(datos);
        return datos;
    }

    //eliminar
    public String delete(Long id){
        this.base.deleteById(id);
        return "eliminado";
    }
}
