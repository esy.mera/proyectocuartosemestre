package erp.pedidos.ciudad;

    import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;

//import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ciudad")
public class ciudadController {

    @Autowired
    public ciudadService service;

    //obtener todos
    @Operation(summary = "Obtiene todas las ciudades, Requiere ciudad-getAll")
    @PreAuthorize("hasAuthority('ciudad-getAll')")
    @GetMapping("/")
    public List<ciudad> getAll(){ //List o ArrayList es lo mismo
        return this.service.getAll();
    }

    //obtener uno
    @Operation(summary = "Obtiene una ciudad por su id, Requiere ciudad-getOne")
    @PreAuthorize("hasAuthority('ciudad-getOne')")
    @GetMapping("/{id}/")
    public Optional<ciudad> getOne(@PathVariable("id") Long id){
        return this.service.getOne(id);
    }

    //agregar
    @Operation(summary = "Agrega una ciudad, Requiere ciudad-save")
    @PreAuthorize("hasAuthority('ciudad-save')")
    @PostMapping("/")
    public ciudad save(@RequestBody ciudad body){ //post
        return this.service.save(body);
    }

    //actualizar campo completo el id va en el body
    @Operation(summary = "Actualizar campo completo de una ciudad, el id va en el body , Requiere ciudad-update")
    @PreAuthorize("hasAuthority('ciudad-update')")
    @PutMapping("/")
    public ciudad update(@RequestBody ciudad entity){
        return this.service.save(entity);
    }

    //actualizar campo completo el id va en la url
    @Operation(summary = "Actualizar campo completo de una ciudad, el id va en la url, Requiere ciudad-put")
    @PreAuthorize("hasAuthority('ciudad-put')")
    @PutMapping("/{id}/")
    public ciudad put( @RequestBody ciudad per, @PathVariable("id") Long id){
        return this.service.put(id,per);
    }

    //actualizar por campos
    @Operation(summary = "Actualizar una ciudad por campos, el id va en la url, Requiere ciudad-patch")
    @PreAuthorize("hasAuthority('ciudad-patch')")
    @PatchMapping("/{id}/")
    public ciudad patch( @RequestBody ciudad per, @PathVariable("id") Long id){
        return this.service.patch(id,per);
    }

    //eliminar
    @Operation(summary = "Elimina una ciudad, el id va en la url, Requiere ciudad-delete")
    @PreAuthorize("hasAuthority('ciudad-delete')")
    @DeleteMapping("/{id}/")
    public String delete(@PathVariable("id") Long id){
        return this.service.delete(id);
    }
}

// al usar @RequestParam se auto configura sea string o integer
    /*@GetMapping("param")
    public String para(@RequestParam("nombre") String nombre){
        return nombre + 1;
        //http://localhost:3000/param?nombre=sebas  imprime sebas
    }

    @GetMapping("param2")
    public Integer para(@RequestParam("num") Integer num){
        return num + 1;
        //http://localhost:3000/param2?num=2  imprime 3
    }*/
