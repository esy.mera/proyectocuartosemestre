package erp.pedidos.compania;

import java.math.BigInteger;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import lombok.Data;
@Data
@Entity
public class compania {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nombreCompania;
    private String direccionCompania;
    private BigInteger telefonoCompania;
    private String correoCompania;


}