import 'package:flutter/material.dart';
import 'package:movile_flutter/scr/gestionarClientes.dart';
import 'package:movile_flutter/scr/gestionarProductos.dart';
import 'package:movile_flutter/scr/inventario.dart';
import 'package:movile_flutter/scr/login.dart';
import 'package:movile_flutter/scr/registrar.dart';
import 'package:movile_flutter/scr/registroProductos.dart';


void main() => runApp( MyApp());

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _routes = {
    '/login': (context)=> login(),
    '/inventario': (context)=> inventario(),
    '/registrar': (context)=> registrar(),
    '/cliente': (context)=> cliente(),
    '/producto': (context)=> registrarproducto(),
    '/gestionar producto': (context)=> producto(),
  };
  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: '/login',
      routes: _routes,
    );
  }
}