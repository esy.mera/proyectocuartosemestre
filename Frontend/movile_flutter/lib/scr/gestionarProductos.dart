import 'package:flutter/material.dart';

class User {
  final int id;
  final String name;
  final String address;
  final int price;

  User(this.id, this.name,  this.address, this.price);
}

class producto extends StatefulWidget {
  @override
  _productoState createState() => _productoState();
}

class _productoState extends State<producto> {
  List<User> users = [
    User(1, 'camara',  'Tec', 150),
    
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gestion de clientes'),
      ),
      drawer: Drawer(
       child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 51, 240, 199),
            ),
            child: Text('Menu'),
            
            
          ),
          ListTile(
            
            title: const Text('Inicio'),
            onTap: () {
              Navigator.popAndPushNamed(context, '/inventario');
            },
          ),
          ListTile(
            
            title: const Text('Gestionar'),
            onTap: () {
              Navigator.popAndPushNamed(context, '');
            },
          ),
          ListTile(
            
            title: const Text('Guardar'),
            onTap: () {
              Navigator.popAndPushNamed(context, '');
            },
          ),
        ],
      ),
    ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: [
            DataColumn(label: Text('ID')),
            DataColumn(label: Text('Nombre')),
            DataColumn(label: Text('Dirección')),
            DataColumn(label: Text('Precio')),
            DataColumn(label: Text('Actions')),
          ],
          rows: users.map((user) {
            return DataRow(cells: [
              DataCell(Text(user.id.toString())),
              DataCell(Text(user.name)),
              DataCell(Text(user.address)),
              DataCell(Text(user.price.toString())),
              DataCell(
                Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        // Acciones al editar el usuario
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Edit User'),
                            content: Text('Edit user ${user.name}'),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Cancel'),
                              ),
                              TextButton(
                                onPressed: () {
                                  // Lógica para guardar los cambios del usuario
                                  Navigator.pop(context);
                                },
                                child: Text('Save'),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        // Acciones al eliminar el usuario
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Delete User'),
                            content: Text('Delete user ${user.name}?'),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Cancel'),
                              ),
                              TextButton(
                                onPressed: () {
                                  // Lógica para eliminar el usuario
                                  setState(() {
                                    users.remove(user);
                                  });
                                  Navigator.pop(context);
                                },
                                child: Text('Delete'),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ]);
          }).toList(),
        ),
      ),
    );
  }
}