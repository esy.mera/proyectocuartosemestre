import 'package:flutter/material.dart';

class inventario extends StatefulWidget {
  @override
  _inventarioState createState() => _inventarioState();
}

class _inventarioState extends State<inventario> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[600],
      body: GridView.count(
        crossAxisCount: 2,
        children: <Widget>[
          Center(
            child: Text(
              'Inventario',
              style: TextStyle(
                fontFamily: 'cursiva',
                fontSize: 30.0,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Color.fromARGB(255, 7, 231, 183).withAlpha(30),
              onTap: () {
                  Navigator.popAndPushNamed(context, "/registrar");
                },
              child: ListTile(
                title: Text(
                  'Gestion de Clientes',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage("assets/grupo.png"),
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: () {
                  Navigator.popAndPushNamed(context, "/producto");
                },
              child: ListTile(
                title: Text(
                  'Gestion de Productos, categorias y reabastecimiento',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage("assets/carrito.png"),
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              
              child: ListTile(
                title: Text(
                  'Gestion de roles y usuarios',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage("assets/persona.png"),
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              
              child: ListTile(
                title: Text(
                  'Gestion de almacen y compania',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage("assets/almacen.png"),
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              
              child: ListTile(
                title: Text(
                  'Gestion de proveedores y de documentos de compra',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage("assets/documento.png"),
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            color: Color.fromARGB(255, 54, 240, 230),
            child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              
              child: ListTile(
                title: Text(
                  'Gestion de factura',
                  textAlign: TextAlign.center,
                ),
                subtitle: Image(
                  image: AssetImage('assets/factura.png'),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}