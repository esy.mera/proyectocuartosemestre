'use client'

import imagen from "@/app/assets/logo_pricipal.png"
import Cookies from "js-cookie";
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { useForm } from "react-hook-form";
import useBasicAuth from "../hooks/inicioSession";


export default function () {

    const { register, handleSubmit, getValues } = useForm();
  const navigation = useRouter();

  const onSubmit = async () => {
        console.log("ingreso en el submit")
        const { bearerToken, usuario } = await useBasicAuth(getValues("user"), getValues("password"));
        Cookies.set("token",bearerToken);
        Cookies.set("usuario",usuario.nombre);
        console.log(usuario.nombre)
        localStorage.setItem("empresa","2")
        navigation.push("/inicio");
  }


    return (
        <div className="hero min-h-screen bg">
            <div className="hero-content flex-col">
                <div>
                    <Image
                        src={imagen}
                        width={230}
                        alt="Picture of the author"
                    />
                </div>
                <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl card-transparentes">

                    <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                        <div className="text-center text-5xl text-white mb-6">
                            <h1>LOGIN</h1>
                        </div>
                        <div className="form-control mb-2">
                            <input type="text" placeholder="Usuario" className="input input-bordered bg-white text-black" {...register("user")} />
                        </div>
                        <div className="form-control">
                            <input type="text" placeholder="Contraseña" className="input input-bordered bg-white text-black" {...register("password")}/>
                        </div>
                        <div className="form-control mt-6">
                            <button type='submit' className="btn boton-color text-black">Iniciar Sesion</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
/*<label className="label">
                                <a href="#" className="label-text-alt link link-hover text-white">¿Olvidaste tu Cotraseña?</a>
                            </label> */
