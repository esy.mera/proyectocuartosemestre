async function fecher(url:string,token:string) {
    const link:string = process.env.API_URL + "/api/" + url;
    return await fetch(link,{
        method: 'DELETE',
        headers:{
            'Authorization': token
        }
    });

}

const useEliminar = async (link:string, token:string) => {
    
        const response = await fecher(link, token);
        
    

   return response
}

export default useEliminar;