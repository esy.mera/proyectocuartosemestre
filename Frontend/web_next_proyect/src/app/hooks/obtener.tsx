import Cookies from "js-cookie";

async function fecher(url:string) {
    const token = Cookies.get("token")|| "";
    const link:string = process.env.API_URL + "/api/" + url;
    let respuesta= await fetch(link,{
        headers: {
            "Authorization": token
          }
    });
    return respuesta

}

const useObtener = async (link:string) => {
    let data = null;
    let error;
    try {
        const response = await fecher(link);
        
        if (response.ok){
            data = await response.json();
          }else {
            error = "Servidor: "+ ((await response.json()).trace);
          }
    } catch (err) {
        error = "Cliente: "+err;
    }

    return{
        data,
        error
    }
}

export default useObtener;