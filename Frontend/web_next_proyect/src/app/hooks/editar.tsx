async function fecher(url:string) {
    const token= localStorage.getItem('token') || "";
    const link:string = process.env.API_URL + "/api/" + url;
    return await fetch(link,{
        method: 'PUT',
        headers:{
            'Authorization': token
        }
    });

}

const useEditar = async (link:string) => {
    let data = null;
    let error;
    try {
        const response = await fecher(link);
        
        if (response.ok){
            data = await response.json();
          }else {
            error = "Servidor: "+ ((await response.json()).trace);
          }
    } catch (err) {
        error = "Cliente: "+err;
    }

    return{
        data,
        error
    }
}

export default useEditar;