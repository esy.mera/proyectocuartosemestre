const fecher  = async (url:string, token: string) => {
    return await fetch(url, {
        method: 'POST',
        headers:{
            'Authorization': token
        }
    })
}

const useBasicAuth = async (username: string, password: string) => {
    let bearerToken = "";
    let usuario:any=""
    
    try {
        
        const token = 'Basic '+ Buffer.from(username+":"+password).toString('base64');
        
        const baseUrl = process.env.API_URL;
        console.log(token)
        console.log(baseUrl)
        
        const response = await fecher(baseUrl+'/login', token);
        
        if (response.ok){
            bearerToken = response.headers.get("Authorization") || "";
            usuario = {
                nombre: username
            };
    
        }
    } catch (error) {
        console.log(error)
    }

    return{
        bearerToken,
        usuario
    }
}

export default useBasicAuth;