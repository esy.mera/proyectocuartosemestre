import Cookies from 'js-cookie';

const fecher = async (url: string, token: string, data:any) => {

    return await fetch(url, {
      method: 'POST',
      headers: { 
        'Authorization' :token,
        'Content-Type': 'application/json',
        'Accept':'application/json'
      },
      body: JSON.stringify(data),      
    });
  }
  
  const agregarBase = async (url: string, body:any) => {
    url=  process.env.API_URL+"/api/"+url;
    const token = Cookies.get('token') || "";
    let data;
    let error;
    try{
      const response = await fecher(url, token, body)
      console.log(response)
      if (response.ok){
        data = await response.json();
        console.log("yessssss")
      }else {
        error = "Servidor: "+ ((await response.json()).trace).substring(1,300);
      }
    }catch (e){
      error = "Cliente: "+e;
    }
    return {
      data,
      error
    }
  
  }

  export default agregarBase;