import Cookies from "js-cookie";


let agregarImgHook= async (body:any)=>{
    const token = Cookies.get('token') || "";
    let response= await fetch('http://localhost:8080/api/images', {
        method: 'POST',
        body: body,
        headers: {
            "Authorization": token
        }
    })

    let com=await response.json()

    return com
}

export default agregarImgHook;