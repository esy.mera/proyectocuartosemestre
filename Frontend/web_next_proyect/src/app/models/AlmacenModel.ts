class almacenModel {
    id: number | undefined;
    capacidad?: number;
    nombreAlmacen?: string;
    direccion?: string;
    ciudad?: string;
    ciudadId?: number;
}