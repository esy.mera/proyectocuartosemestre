class datosProductos {
    id: number | undefined;
    nombre?: string;
    descripcion?: string;
    stock?: number;
    precio?: number;
    cantidad?: number;
    proveedor?: string;
    categoria?: string;
    imagen?: string;
    tipoImagen?: string;
    idImagen?:number;
    nombreImg?:string;
    categoriaId?:number;
    proveedorId?:number;
}