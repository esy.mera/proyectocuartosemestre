import useObtener from "@/app/hooks/obtener";

let categorias=async ()=>{

    let datos= await useObtener("categorias/")
    return datos

}

export default categorias;