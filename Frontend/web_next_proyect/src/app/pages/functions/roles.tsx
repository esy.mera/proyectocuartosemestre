//role/

import useObtener from "@/app/hooks/obtener";

let roles=async ()=>{

    let datos= await useObtener("role/")
    return datos

}

export default roles;