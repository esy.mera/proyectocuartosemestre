import useObtener from "@/app/hooks/obtener";

let ciudades=async ()=>{

    let datos= await useObtener("ciudad/")
    return datos

}

export default ciudades;