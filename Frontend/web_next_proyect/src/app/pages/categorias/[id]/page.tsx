"use client";

import imagenLogo from "@/app/assets/logo_pricipal.png"
import editar from "@/app/assets/edit.svg"
import eliminar from "@/app/assets/eliminar.svg"
import mas from "@/app/assets/mas.svg"
import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import Agregar from "../components/agregar";
import Editarform from "../components/edit";
import { MostrarVentanas } from "../../functions/GlobalFunctions";
import Navbar from "../../GlobalComponents/navbar";
import enlistarcategoria from "../services/service";
import eliminarCategoria from "../services/eliminarService";


function usuarios(param: any) {
    let parametroId = Number(param.params.id)
    const editarPantalla = useRef<HTMLDivElement>(null);
    const AgregarPantalla = useRef<HTMLDivElement>(null);

    const [datosCategoria, setDatosCategoria] = useState<categoriaModel[]>()
    const [categoriaEdit, setCtegoriaEdit] = useState<categoriaModel>({
        id:0,
        descripcion:"",
        nombre:""
    })

    let MostrarAgregar = () => {
        MostrarVentanas(AgregarPantalla)
    }
    let MostrarEditar = () => {
        MostrarVentanas(editarPantalla)
    }

    let cargaDatos = async () => {
        setDatosCategoria(await enlistarcategoria(parametroId))
    }

    useEffect(() => {
        cargaDatos()
    }, [])


    return (
        <><div className="absolute bg w-full h-screen overflow-y-scroll">


            <Navbar />


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo" />
                <div className="text-center text-4xl text-white">Gestionar Categorias</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="overflow-x-auto text-white bg-white">
                    <table className="table text-black ocultar">
                        {/* head */}
                        <thead className=" text-white">
                            <tr className="bg-gray-700 text-center">
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* row 1 */}
                            <tr>

                                <th>
                                    <button className="btn btn-ghost hover:bg-blue-600" onClick={MostrarAgregar}><Image
                                        src={mas}
                                        width={23}
                                        alt="logo" /></button>
                                </th>
                            </tr>

                            {
                                datosCategoria?.map((datos, index) => {
                                    return (<tr key={index}>
                                        <td >
                                            <div className="flex items-center space-x-3">
                                                
                                                <div>
                                                    <div className="font-bold">{datos.nombre}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {datos.descripcion}
                                        </td>
                                        <th className="flex justify-center ">
                                            <button className="btn btn-ghost hover:bg-yellow-400" onClick={()=>{
                                                    setCtegoriaEdit({
                                                        id:datos.id,
                                                        nombre:datos.nombre,
                                                        descripcion:datos.descripcion
                                                    })
                                                    MostrarEditar()

                                                }}><Image
                                                src={editar}
                                                alt="logo" /></button>
                                        </th>
                                        <th>
                                            <button className="btn btn-ghost hover:bg-red-600" ><Image
                                                src={eliminar}
                                                alt="logo" onClick={async ()=>{
                                                    await eliminarCategoria(datos.id)
                                                    cargaDatos()
                                                }}/></button>
                                        </th>
                                    </tr>)
                                })
                            }

                            {/* row 2 */}

                            




                        </tbody>

                    </table>

                </div>

            </div>
            <div className="flex justify-center pb-20">
                <div className="join bg">
                    <button className="join-item btn">«</button>
                    <button className="join-item btn">Pagina {parametroId}</button>
                    <button className="join-item btn">»</button>
                </div>
            </div>

        </div>

            <Agregar datos={{ miRef: AgregarPantalla, agregar: MostrarAgregar, cargarDatos: cargaDatos }} />
            <Editarform datos={{ miRef: editarPantalla, editar: MostrarEditar, cargarDatos: cargaDatos, categoriaEdit:categoriaEdit }} />





        </>
    );
}

export default usuarios;