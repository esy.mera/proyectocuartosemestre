
import { useForm } from "react-hook-form";
import agregarCategoria from "../services/agregarService";
import { useEffect, useState } from "react";

const editarform = (props: any) => {
    const { miRef, editar, cargarDatos, categoriaEdit} = props.datos;
    const { register, handleSubmit, reset } = useForm();

    const [nombre, setNombre]= useState<any>()
    const [descripcion, setDescripcion]= useState<any>()

    useEffect(()=>{
        setNombre(categoriaEdit.nombre)
        setDescripcion(categoriaEdit.descripcion)

    },[categoriaEdit])
    
    const onSubmit = async () => {

        let categoria={
            id:categoriaEdit.id,
            nombre: nombre,
            descripcion: descripcion
          }

          console.log(categoria)
        await agregarCategoria(categoria)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        editar()
        reset()

    }

    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado ">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Editar Categoria</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" value={nombre} onChange={(e) => {
                        const campo = e.target.value;
                        setNombre(campo)
                    }} />
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Descripcion" className="input input-bordered bg-white text-black" value={descripcion} onChange={(e) => {
                        const campo = e.target.value;
                        setDescripcion(campo)
                    }} />
                    </div>
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default editarform