import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarCategoria=async (id:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`categorias/${id}/`,token)

}
export default eliminarCategoria