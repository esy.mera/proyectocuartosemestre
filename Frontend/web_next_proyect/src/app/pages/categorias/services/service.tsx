import useObtener from "@/app/hooks/obtener"

const enlistarcategoria = async (pagina:number) => {
    let datosDefinitivos = [];

    let datos = await useObtener(`categorias/categorias/${pagina}/`);
    let entregaData = datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto: categoriaModel = {
            id: 0,
            nombre: "",
            descripcion: "",
        }

        objeto.id = entregaData[i][0]
        objeto.nombre = entregaData[i][2]
        objeto.descripcion = entregaData[i][1]

        datosDefinitivos.push(objeto)

    }
    return datosDefinitivos


}
export default enlistarcategoria;