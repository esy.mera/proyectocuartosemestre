import useObtener from "@/app/hooks/obtener";
import Cookies from "js-cookie";

 let userPricipal=async ()=>{
    const usuario = Cookies.get('usuario') || "";
    let datos=await useObtener(`user/user/${usuario}`)
    return datos.data
}

export default userPricipal