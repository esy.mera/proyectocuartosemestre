import useObtener from "@/app/hooks/obtener"
import Cookies from "js-cookie";

export let notificaciones= async ()=>{
    let empresa = Number(localStorage.getItem("empresa"));
    let datos= await useObtener(`producto/notificaciones/${empresa}/`)
    return datos.data;
}

