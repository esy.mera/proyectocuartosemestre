"use client"

import Image from 'next/image'
import imagenPersona from "@/app/assets/persona.png"
import imagenZapato from "@/app/assets/zapato fly.png"
import campana from "@/app/assets/campana.png"
import amburguesa from "@/app/assets/list.svg"
import home from "@/app/assets/house-door-fill.svg"
import usuario from "@/app/assets/person-circle.svg"
import categorias from "@/app/assets/journal-text.svg"
import productos from "@/app/assets/cart4.svg"
import proveedores from "@/app/assets/people-fill.svg"
import almacen from "@/app/assets/shop-window.svg"
import { useEffect, useState } from 'react'
import { notificaciones } from './notificacines'
import { useRouter } from 'next/navigation'
import useObtener from '@/app/hooks/obtener'
import Cookies from 'js-cookie'



const navbar = () => {
    const ruta=useRouter()
    const [notificacionesList, setNotificacionesList] = useState<any[]>([]);
    const [userPrincipal, setUserPrincipal] = useState<any>({});
    let CargaDatos = async () => {
        const datos = await notificaciones()
        setNotificacionesList(datos)
        
        
    }

    async function datosUser() {
        const usuario = Cookies.get('usuario') || "";
        const datosUser=await useObtener(`user/user/${usuario}`)
        let objeto;
        for (let i = 0; i < datosUser.data.length; i++) {
            objeto={
                nombre:datosUser.data[i][4]
            }
            
        }
         setUserPrincipal(objeto)
    }

    useEffect(() => {
        CargaDatos()
        datosUser()

    }, [])


    return (
        <div className="navbar bg-white sticky top-0 z-40 border border-black border-b-3">
            <div className="drawer">
                <input id="my-drawer-4" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content flex items-center">
                    {/* Page content here */}
                    <label htmlFor="my-drawer-4" className="drawer-button btn btn-ghost"> <Image
                        src={amburguesa}
                        width={40}
                        alt="logo" /> </label>
                    <div className="mx-3">
                        <a href="">
                            <Image
                                src={imagenZapato}
                                width={70}
                                alt="logo" />
                        </a>

                    </div>
                </div>
                {/*items de drawer */}
                <div className="drawer-side z-50">
                    <label htmlFor="my-drawer-4" className="drawer-overlay"></label>
                    <ul className="menu p-4 w-80 h-full bg-drawe text-white ">
                        {/* Sidebar content here */}
                        <li className="mx-auto my-3">GESTIONES</li>

                        <li><a className="hover:text-black" onClick={()=>{
                            ruta.replace("/inicio")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={home}
                                    width={35}
                                    alt="logo" />
                            </span> Inicio</a>
                        </li>
                        <li><a className="hover:text-black" onClick={()=>{
                            ruta.replace("/pages/roles/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={imagenPersona}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Roles</a>
                        </li>

                        <li><a className="hover:text-black" onClick={()=>{
                            ruta.replace("/pages/usuarios/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={usuario}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Usuarios</a>
                        </li>

                        <li><a className="hover:text-black" onClick={()=>{
                            ruta.replace("/pages/productos/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={productos}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Productos</a>
                        </li>

                        <li><a className="hover:text-black"onClick={()=>{
                            ruta.replace("/pages/categorias/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={categorias}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Categorias</a>
                        </li>
                        <li><a className="hover:text-black"onClick={()=>{
                            ruta.replace("/pages/proveedores/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={proveedores}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Proveedores</a>
                        </li>
                        <li><a className="hover:text-black" onClick={()=>{
                            ruta.replace("/pages/almacenes/1")
                        }}>
                            <span className="me-3">
                                <Image
                                    src={almacen}
                                    width={35}
                                    alt="logo" />
                            </span> Gestion Almacen</a>
                        </li>
                    </ul>
                </div>

                {/*fin items de drawer */}
            </div>

            <label className=" flex-1">

            </label>
            <div className="flex-none">
                <div className="dropdown dropdown-end">
                    <label tabIndex={0} className="btn btn-ghost flex">
                        <div className="w-10 rounded-full">
                            <Image
                                src={imagenPersona}
                                alt="logo" />

                        </div>
                        <div className="text-black">
                            { userPrincipal.nombre}
                        </div>
                    </label>
                    <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52">
                        <button className="btn boton-color text-black" onClick={()=>{
                            Cookies.remove("token")
                            Cookies.remove("usuario")
                            localStorage.removeItem("empresa")
                            ruta.replace("/login")
                        }}><a>Cerrar sessión</a></button>
                    </ul>
                </div>
                <div className="dropdown dropdown-end">
                    <label tabIndex={0} className="btn btn-ghost mx-3">
                        <div className="indicator">
                            <Image
                                src={campana}
                                width={25}
                                alt="logo" />
                            <span className="badge badge-sm badge-error indicator-item z-0">{notificacionesList.length}</span>
                        </div>
                    </label>
                    <ul tabIndex={0} className=" menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-96">

                        {
                            notificacionesList.map((dato,index) => {
                                return (
                                    <li key={index}>
                                        <div className="alert alert-warning mt-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" /></svg>
                                            <span>Producto insuficiente: {dato[1]}</span>
                                        </div>
                                    </li>
                                )
                            })
                        }



                    </ul>
                </div>
            </div>
        </div>
    );
}


export default navbar