"use client";

import imagenLogo from "@/app/assets/logo_pricipal.png"
import editar from "@/app/assets/edit.svg"
import eliminar from "@/app/assets/eliminar.svg"
import mas from "@/app/assets/mas.svg"
import almacen from "@/app/assets/house-fill.svg"
import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import Agregar from "../components/agregar";
import Editarform from "../components/edit";
import { MostrarVentanas } from "../../functions/GlobalFunctions";
import Navbar from "../../GlobalComponents/navbar";
import enlistarAlmacen from "../service/services";
import eliminarAlmacen from "../service/eliminarService";


function usuarios(param: any) {
    let parametroId = Number(param.params.id)
    const editarPantalla = useRef<HTMLDivElement>(null);
    const AgregarPantalla = useRef<HTMLDivElement>(null);

    const [datosAlmacen, setDatosAlmacen] = useState<almacenModel[]>()
    const [almacenEdit, setAlmacenEdit] = useState<almacenModel>({
        id: 0,
        capacidad: 0,
        ciudad: "",
        ciudadId: 0,
        direccion: "",
        nombreAlmacen: ""
    })

    let MostrarAgregar = () => {
        MostrarVentanas(AgregarPantalla)
    }
    let MostrarEditar = () => {
        MostrarVentanas(editarPantalla)
    }
    let cargaDatos = async () => {
        setDatosAlmacen(await enlistarAlmacen(parametroId))
    }

    useEffect(() => {
        cargaDatos()
    }, [])

    return (
        <><div className="absolute bg w-full h-screen overflow-y-scroll">


            <Navbar />


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo" />
                <div className="text-center text-4xl text-white">Gestionar Almacenes</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="overflow-x-auto text-white bg-white">
                    <table className="table text-black ocultar">
                        {/* head */}
                        <thead className=" text-white">
                            <tr className="bg-gray-700 text-center">
                                <th>Nombre de Almacen</th>
                                <th>Dirreccion</th>
                                <th>Capacidad</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* row 1 */}
                            <tr>

                                <th>
                                    <button className="btn btn-ghost hover:bg-blue-600" onClick={MostrarAgregar}><Image
                                        src={mas}
                                        width={23}
                                        alt="logo" /></button>
                                </th>
                            </tr>

                            {
                                datosAlmacen?.map((datos, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>
                                                <div className="flex items-center space-x-3">
                                                    <div className="avatar">
                                                        <div className="mask mask-squircle w-12 h-12">
                                                            <Image
                                                                src={almacen}
                                                                alt="logo" />
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div className="font-bold">{datos.nombreAlmacen}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {datos.direccion}

                                                <br />
                                                <span className="badge badge-ghost badge-sm">{datos.ciudad}</span>
                                            </td>
                                            <td>{datos.capacidad}</td>
                                            <th className="flex justify-center ">
                                                <button className="btn btn-ghost hover:bg-yellow-400" onClick={() => {
                                                    setAlmacenEdit({
                                                        id: datos.id,
                                                        capacidad: datos.capacidad,
                                                        ciudad: datos.ciudad,
                                                        ciudadId: datos.ciudadId,
                                                        direccion: datos.direccion,
                                                        nombreAlmacen: datos.nombreAlmacen
                                                    })
                                                    MostrarEditar()

                                                }}><Image
                                                        src={editar}
                                                        alt="logo" /></button>
                                            </th>
                                            <th>
                                                <button className="btn btn-ghost hover:bg-red-600" ><Image
                                                    src={eliminar}
                                                    alt="logo" onClick={async () => {
                                                        await eliminarAlmacen(datos.id)
                                                        cargaDatos()
                                                    }} /></button>
                                            </th>
                                        </tr>
                                    )
                                })
                            }


                            {/* row 2 */}






                        </tbody>

                    </table>

                </div>

            </div>
            <div className="flex justify-center pb-20">
                <div className="join bg">
                    <button className="join-item btn">«</button>
                    <button className="join-item btn">Pagina {parametroId}</button>
                    <button className="join-item btn">»</button>
                </div>
            </div>

        </div>

            <Agregar datos={{ miRef: AgregarPantalla, agregar: MostrarAgregar, cargarDatos: cargaDatos }} />
            <Editarform datos={{ miRef: editarPantalla, editar: MostrarEditar, cargarDatos: cargaDatos, almacenEdit:almacenEdit }} />





        </>
    );
}

export default usuarios;