
import { useForm } from "react-hook-form";
import agregarAlmacen from "../service/agregarservice";
import { useEffect, useState } from "react";
import ciudades from "../../functions/Ciudades";

const editarform = (props: any) => {
    const [ciudadesData,setCiudadesData]=useState<any[]>([])

    let carga=async ()=>{
        let data= await ciudades()

        setCiudadesData(data.data);
    }
    useEffect(()=>{
        carga()
    },[])
    const { miRef, editar, cargarDatos, almacenEdit} = props.datos;
    const { register, handleSubmit, reset } = useForm();

    const [nombre, setNombre]= useState<any>()
    const [direccion, setDireccion]= useState<any>()
    const [capacidad, setCapacidad]= useState<any>()
    const [ciudad, setCiudad]= useState<any>()

    useEffect(()=>{
        setNombre(almacenEdit.nombreAlmacen)
        setDireccion(almacenEdit.direccion)
        setCiudad(almacenEdit.ciudadId)
        setCapacidad(almacenEdit.capacidad)
    },[almacenEdit])

    const onSubmit = async (data: any) => {
        
        let empresa = Number(localStorage.getItem("empresa"));

        let almacen={
            id:almacenEdit.id,
            nombre_almacen: nombre,
            direccion_almacen: direccion,
            capacidad_almacenamiento: capacidad,
            idCompania: {
              id: empresa
            },
            idCiudad: {
              id: Number(ciudad)
            }
          }
          console.log(almacen)
        await agregarAlmacen(almacen)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        editar()
        reset()

    }
    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado ">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Editar Almacen</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre de Almacen" className="input input-bordered bg-white text-black" value={nombre} onChange={(e) => {
                        const campo = e.target.value;
                        setNombre(campo)
                    }}/>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Direccion" className="input input-bordered bg-white text-black" value={direccion} onChange={(e) => {
                        const campo = e.target.value;
                        setDireccion(campo)
                    }}/>
                    </div>
                    <div className="form-control mb-2">
                        <input type="number" placeholder="Capacidad del Almacen" className="input input-bordered bg-white text-black" value={capacidad} onChange={(e) => {
                        const campo = e.target.value;
                        setCapacidad(campo)
                    }}/>
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} value={ciudad} onChange={(e) => {
                        const campo = e.target.value;
                        setCiudad(campo)
                    }}>
                        <option value={""} disabled>Ciudades</option>
                        {
                            ciudadesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombre}</option>
                                )
                            })
                        }

                    </select>
                    
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default editarform