import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarAlmacen=async (id:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`almacen/${id}/`,token)

}
export default eliminarAlmacen