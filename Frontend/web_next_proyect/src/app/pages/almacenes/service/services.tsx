import useObtener from "@/app/hooks/obtener"

const enlistarAlmacen = async (pagina:number) => {
    let empresa = Number(localStorage.getItem("empresa"));
    let datosDefinitivos = [];

    let datos = await useObtener(`almacen/empresa/${empresa}/${pagina}`);
    let entregaData = datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto: almacenModel = {
            id: 0,
            capacidad: 0,
            nombreAlmacen: "",
            direccion: "",
            ciudad: "",
            ciudadId: 0
        }

        objeto.id = entregaData[i][0]
        objeto.capacidad = entregaData[i][1]
        objeto.nombreAlmacen = entregaData[i][2]
        objeto.direccion = entregaData[i][3]
        objeto.ciudad = entregaData[i][4]
        objeto.ciudadId = entregaData[i][5]

        datosDefinitivos.push(objeto)

    }
    return datosDefinitivos


}
export default enlistarAlmacen;