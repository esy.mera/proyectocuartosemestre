import useObtener from "@/app/hooks/obtener"

const enlistar = async (pagina:number) => {
    let empresa = Number(localStorage.getItem("empresa"));
    let datosDefinitivos = [];

    let datos = await useObtener(`user/empresa/${empresa}/${pagina}`);
    let entregaData = datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto: userModel = {
            idUsuario: 0,
            role: "",
            nombre: "",
            userName: "",
            Idrole: 0
        }

        objeto.idUsuario = entregaData[i][0]
        objeto.role = entregaData[i][1]
        objeto.nombre = entregaData[i][2]
        objeto.userName = entregaData[i][3]
        objeto.Idrole = entregaData[i][4]

        datosDefinitivos.push(objeto)

    }
    return datosDefinitivos


}
export default enlistar;