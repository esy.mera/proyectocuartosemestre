import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarusuario=async (id:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`user/${id}/`,token)

}
export default eliminarusuario