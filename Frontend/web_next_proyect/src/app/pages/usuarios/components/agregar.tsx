import { useForm } from "react-hook-form";
import agregarusuario from "../services/agregarService";
import { useEffect, useState } from "react";
import roles from "../../functions/roles";

const agregar = (props: any) => {
    const [rolesData,setRolesData]=useState<any[]>([])

    let carga=async ()=>{
        let data= await roles()

        setRolesData(data.data);
    }
    useEffect(()=>{
        carga()
    },[])
    const { miRef, agregar, cargarDatos} = props.datos;
    const { register, handleSubmit, reset } = useForm();
    const onSubmit = async (data: any) => {
        
        let empresa = Number(localStorage.getItem("empresa"));

        let usuario={
            password: data.Contrasena,
            name: data.Nombre,
            username: data.NombreUsuario,
            compania: {
              id: empresa,
            },
            roles: [
              {
                id: Number(data.roles),
              }
            ]
          }
          console.log(usuario)
        await agregarusuario(usuario)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        agregar()
        reset()

    }
    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Agregar Usuario</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" {...register('Nombre')}/>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre de Usuario" className="input input-bordered bg-white text-black" {...register('NombreUsuario')}/>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Contraseña" className="input input-bordered bg-white text-black" {...register('Contrasena')}/>
                    </div>
                    
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} {...register('roles')}>
                        <option value={""} disabled>Roles</option>
                        {
                            rolesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.name}</option>
                                )
                            })
                        }

                    </select>

                    
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default agregar