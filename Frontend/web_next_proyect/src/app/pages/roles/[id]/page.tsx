"use client";

import imagenLogo from "@/app/assets/logo_pricipal.png"
import editar from "@/app/assets/edit.svg"
import eliminar from "@/app/assets/eliminar.svg"
import mas from "@/app/assets/mas.svg"
import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import Agregar from "../components/agregar";
import Editarform from "../components/edit";
import { MostrarVentanas } from "../../functions/GlobalFunctions";
import Navbar from "../../GlobalComponents/navbar";
import enlistarRol from "../services/service";
import rol from "@/app/assets/person-badge-fill.svg"
import eliminarRole from "../services/eliminarService";


function usuarios(param: any) {
    let parametroId = Number(param.params.id)
    const editarPantalla = useRef<HTMLDivElement>(null);
    const AgregarPantalla = useRef<HTMLDivElement>(null);

    const [datosRol, setDatosrol] = useState<rolModel[]>()
    const [rolEdit, setRolEdit] = useState<rolModel>({
        id:0,
        enabled:true,
        name:""
    })


    let MostrarAgregar = () => {
        MostrarVentanas(AgregarPantalla)
    }
    let MostrarEditar = () => {
        MostrarVentanas(editarPantalla)
    }

    let cargaDatos = async () => {
        setDatosrol(await enlistarRol(parametroId))
    }

    useEffect(() => {
        cargaDatos()
    }, [])


    return (
        <><div className="absolute bg w-full h-screen overflow-y-scroll">


            <Navbar />


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo" />
                <div className="text-center text-4xl text-white">Gestionar Roles</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="overflow-x-auto text-white bg-white">
                    <table className="table text-black ocultar">
                        {/* head */}
                        <thead className=" text-white">
                            <tr className="bg-gray-700 text-center">
                                <th>Nombre</th>
                                <th>enabled</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* row 1 */}
                            <tr>

                                <th>
                                    <button className="btn btn-ghost hover:bg-blue-600" onClick={MostrarAgregar}><Image
                                        src={mas}
                                        width={23}
                                        alt="logo" /></button>
                                </th>
                            </tr>
                            {
                                datosRol?.map((datos, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>
                                                <div className="flex items-center space-x-3">
                                                    <div className="avatar">
                                                        <div className="mask mask-squircle w-12 h-12">
                                                            <Image
                                                                src={rol}
                                                                alt="logo" />
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div className="font-bold">{datos.name}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {JSON.stringify(datos.enabled) }
                                            </td>
                                            <th className="flex justify-center ">
                                                <button className="btn btn-ghost hover:bg-yellow-400" onClick={()=>{
                                                    setRolEdit({
                                                        id:datos.id,
                                                        enabled:datos.enabled,
                                                        name:datos.name
                                                    })
                                                    MostrarEditar()

                                                }}><Image
                                                    src={editar}
                                                    alt="logo" /></button>
                                            </th>
                                            <th>
                                                <button className="btn btn-ghost hover:bg-red-600" ><Image
                                                    src={eliminar}
                                                    alt="logo" onClick={async ()=>{
                                                        await eliminarRole(datos.id)
                                                        cargaDatos()
                                                    }}/></button>
                                            </th>
                                        </tr>
                                    )
                                })
                            }

                            {/* row 2 */}

                           




                        </tbody>

                    </table>

                </div>

            </div>
            <div className="flex justify-center pb-20">
                <div className="join bg">
                    <button className="join-item btn">«</button>
                    <button className="join-item btn">Pagina {parametroId}</button>
                    <button className="join-item btn">»</button>
                </div>
            </div>

        </div>

            <Agregar datos={{ miRef: AgregarPantalla, agregar: MostrarAgregar, cargarDatos: cargaDatos }} />
            <Editarform datos={{ miRef: editarPantalla, editar: MostrarEditar, cargarDatos: cargaDatos, datosEdit: rolEdit }} />





        </>
    );
}

export default usuarios;