import { useForm } from "react-hook-form";
import agregarRole from "../services/agregarService";

const agregar = (props: any) => {
    const { miRef, agregar, cargarDatos} = props.datos;
    const { register, handleSubmit, reset } = useForm();
    const onSubmit = async (data: any) => {

        let role={
            name: data.nombre,
            enabled: JSON.parse(data.enabled),
          }
          console.log(role)
        await agregarRole(role)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        agregar()
        reset()

    }
    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-5xl text-white mb-6">
                        <h1>Agregar Rol</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre del Role" className="input input-bordered bg-white text-black" {...register('nombre')}/>
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} {...register('enabled')}>
                        <option value="" disabled>Enabled</option>
                        <option value={"true"}>True</option>
                        <option value={"false"}>False</option>

                    </select>
                    
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien" >Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default agregar