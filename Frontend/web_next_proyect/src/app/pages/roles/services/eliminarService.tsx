import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarRole=async (id:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`role/${id}/`,token)

}
export default eliminarRole