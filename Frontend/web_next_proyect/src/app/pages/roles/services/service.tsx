import useObtener from "@/app/hooks/obtener"

const enlistarRol = async (pagina:number) => {
    let datosDefinitivos = [];

    let datos = await useObtener(`role/empresa/${pagina}`);
    let entregaData = datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto: rolModel = {
            id: 0,
            name: "",
            enabled: false
        }

        objeto.id = entregaData[i][0]
        objeto.name = entregaData[i][1]
        objeto.enabled = entregaData[i][2]


        datosDefinitivos.push(objeto)

    }
    return datosDefinitivos


}
export default enlistarRol;