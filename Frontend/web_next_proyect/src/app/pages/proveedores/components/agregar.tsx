import { useForm } from "react-hook-form";
import agregarProveedor from "../services/agregarService";
import ciudades from "../../functions/Ciudades";
import { useEffect, useState } from "react";
import almacenes from "../../functions/almacenes";

const agregar = (props: any) => {
    const [ciudadesData,setCiudadesData]=useState<any[]>([])
    const [almacenesData,setAlmacenesData]=useState<almacenModel[]>([])

    let carga=async ()=>{
        let data= await ciudades()
        setCiudadesData(data.data);
        let admacenes= await almacenes()
        setAlmacenesData(admacenes)
    }
    useEffect(()=>{
        carga()
    },[])
    console.log(almacenesData)

    const { miRef, agregar, cargarDatos} = props.datos;
    const { register, handleSubmit, reset } = useForm();
    const onSubmit = async (data: any) => {

        let proveedor={
            nombre: data.nombre,
            direccion: data.direccion,
            ciudad: {
                id: Number(data.ciudad)
            },
            idAlmacen: {
                id: Number(data.almacen)
            }
        }
        await agregarProveedor(proveedor)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        agregar()
        reset()

    }
    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Agregar Proveedores</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" {...register('nombre')}/>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Direccion" className="input input-bordered bg-white text-black" {...register('direccion')}/>
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} {...register('ciudad')}>
                        <option value={""} disabled>Ciudades</option>
                        {
                            ciudadesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombre}</option>
                                )
                            })
                        }

                    </select>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} {...register('almacen')}>
                        <option value={""} disabled>Almacenes</option>
                        {
                            almacenesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombreAlmacen}</option>
                                )
                            })
                        }

                    </select>
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default agregar