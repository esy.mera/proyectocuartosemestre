
import { useForm } from "react-hook-form";
import agregarProveedor from "../services/agregarService";
import { useEffect, useState } from "react";
import ciudades from "../../functions/Ciudades";
import almacenes from "../../functions/almacenes";

const editarform = (props: any) => {
    const [ciudadesData,setCiudadesData]=useState<any[]>([])
    const [almacenesData,setAlmacenesData]=useState<almacenModel[]>([])

    let carga=async ()=>{
        let data= await ciudades()
        setCiudadesData(data.data);
        let admacenes= await almacenes()
        setAlmacenesData(admacenes)
    }
    useEffect(()=>{
        carga()
    },[])
    const { miRef, editar, cargarDatos, proveedorEdit } = props.datos;
    const {handleSubmit, reset } = useForm();
    const [nombre, setNombre]= useState<any>()
    const [direccion, setDireccion]= useState<any>()
    const [ciudad, setCiudad]= useState<any>()
    const [almacen, setAlmacen]= useState<any>()
    useEffect(()=>{
        setNombre(proveedorEdit.nombre)
        setDireccion(proveedorEdit.direccion)
        setCiudad(proveedorEdit.Idciudad)
        setAlmacen(proveedorEdit.Idalmacen)
    },[proveedorEdit])

    const onSubmit = async () => {

        let proveedor = {
            idProveedor: proveedorEdit.id_proveedor,
            nombre: nombre,
            direccion: direccion,
            ciudad: {
                id: Number(ciudad)
            },
            idAlmacen: {
                id: Number(almacen)
            }
        }

        await agregarProveedor(proveedor)
        await cargarDatos()
        cancelar()
    };

    const cancelar = () => {
        editar()
        reset()

    }
    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">

                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Editar Proveedores</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" value={nombre} onChange={(e) => {
                        const campo = e.target.value;
                        setNombre(campo)
                    }} />
                    </div>
                    <div className="form-control mb-2">
                        <input type="text" placeholder="Direccion" className="input input-bordered bg-white text-black" value={direccion} onChange={(e) => {
                        const campo = e.target.value;
                        setDireccion(campo)
                    }} />
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} value={ciudad} onChange={(e) => {
                        const campo = e.target.value;
                        setCiudad(campo)
                    }}>
                        <option value={""} disabled>Ciudades</option>
                        {
                            ciudadesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombre}</option>
                                )
                            })
                        }
                        <option value={1}>Quito</option>
                        <option value={2}>Guayaquil</option>

                    </select>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} value={almacen} onChange={(e) => {
                        const campo = e.target.value;
                        setAlmacen(campo)
                    }}>
                        <option value={""} disabled>Almacenes</option>
                        {
                            almacenesData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombreAlmacen}</option>
                                )
                            })
                        }

                    </select>
                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default editarform