import useObtener from "@/app/hooks/obtener"

const enlistarProveedores= async(pagina:number)=>{
    let empresa = Number(localStorage.getItem("empresa"));
    let datosDefinitivos=[];
    
    let datos= await useObtener(`proveedores/empresa/${empresa}/${pagina}`);
    let entregaData= datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto:proveedorModel={
            id_proveedor:0,
            direccion:"",
            nombre:"",
            cuidad:"",
            almacen:"",
            Idciudad:0,
            Idalmacen:0
            
        }
        
        objeto.id_proveedor=entregaData[i][0]
        objeto.direccion=entregaData[i][1]
        objeto.nombre=entregaData[i][2]
        objeto.cuidad=entregaData[i][3]
        objeto.almacen=entregaData[i][4]
        objeto.Idciudad=entregaData[i][5]
        objeto.Idalmacen=entregaData[i][6]


        datosDefinitivos.push(objeto)
        
    }
    return datosDefinitivos

    
}
export default enlistarProveedores;