import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarProveedor=async (id:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`proveedores/${id}/`,token)

}
export default eliminarProveedor