"use client";

import imagenLogo from "@/app/assets/logo_pricipal.png"
import editar from "@/app/assets/edit.svg"
import eliminar from "@/app/assets/eliminar.svg"
import mas from "@/app/assets/mas.svg"
import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import Agregar from "../components/agregar";
import Editarform from "../components/edit";
import { MostrarVentanas } from "../../functions/GlobalFunctions";
import Navbar from "../../GlobalComponents/navbar";
import enlistarProveedores from "../services/service";
import usuario from "@/app/assets/person-fill.svg"
import eliminarProveedor from "../services/eliminarService";


function usuarios(param: any) {
    let parametroId = Number(param.params.id)
    const editarPantalla = useRef<HTMLDivElement>(null);
    const AgregarPantalla = useRef<HTMLDivElement>(null);
    const [proveedoresData, setProveedoresData] = useState<proveedorModel[]>()
    const [proveedorEdit, setProveedorEdit] = useState<proveedorModel>({
        id_proveedor:0,
        almacen:"",
        cuidad:"",
        direccion:"",
        Idalmacen:0,
        Idciudad:0,
        nombre:""
    })


    let MostrarAgregar = () => {
        MostrarVentanas(AgregarPantalla)
    }
    let MostrarEditar = () => {
        MostrarVentanas(editarPantalla)
    }
    let cargaDatos = async () => {
        setProveedoresData(await enlistarProveedores(parametroId))

    }
    useEffect(() => {
        cargaDatos()
    }, [])


    return (
        <><div className="absolute bg w-full h-screen overflow-y-scroll">


            <Navbar />


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo" />
                <div className="text-center text-4xl text-white">Gestionar Proveedores</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="overflow-x-auto text-white bg-white">
                    <table className="table text-black ocultar">
                        {/* head */}
                        <thead className=" text-white">
                            <tr className="bg-gray-700 text-center">
                                <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Almacen</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* row 1 */}
                            <tr>

                                <th>
                                    <button className="btn btn-ghost hover:bg-blue-600" onClick={MostrarAgregar}><Image
                                        src={mas}
                                        width={23}
                                        alt="logo" /></button>
                                </th>
                            </tr>

                            {
                                proveedoresData?.map((data, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>
                                                <div className="flex items-center space-x-3">
                                                    <div className="avatar">
                                                        <div className="mask mask-squircle w-12 h-12">
                                                            <Image
                                                                src={usuario}
                                                                alt="logo" />
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div className="font-bold">{data.nombre}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {data.direccion}
                                                <br />
                                                <span className="badge badge-ghost badge-sm">{data.cuidad}</span>
                                            </td>
                                            <td>{data.almacen}</td>
                                            <th className="flex justify-center ">
                                                <button className="btn btn-ghost hover:bg-yellow-400" onClick={()=>{
                                                    setProveedorEdit({
                                                        id_proveedor:data.id_proveedor,
                                                        direccion:data.direccion,
                                                        Idalmacen:data.Idalmacen,
                                                        Idciudad:data.Idciudad,
                                                        nombre:data.nombre
                                                    })
                                                    MostrarEditar()

                                                }}><Image
                                                    src={editar}
                                                    alt="logo" /></button>
                                            </th>
                                            <th>
                                                <button className="btn btn-ghost hover:bg-red-600" ><Image
                                                    src={eliminar}
                                                    alt="logo" onClick={async ()=>{
                                                        await eliminarProveedor(data.id_proveedor)
                                                        cargaDatos()
                                                    }}/></button>
                                            </th>
                                        </tr>
                                    )
                                })
                            }

                            {/* row 2 */}

                            




                        </tbody>

                    </table>

                </div>

            </div>
            <div className="flex justify-center pb-20">
                <div className="join bg">
                    <button className="join-item btn">«</button>
                    <button className="join-item btn">Pagina {parametroId}</button>
                    <button className="join-item btn">»</button>
                </div>
            </div>

        </div>

            <Agregar datos={{ miRef: AgregarPantalla, agregar: MostrarAgregar, cargarDatos: cargaDatos }} />
            <Editarform datos={{ miRef: editarPantalla, editar: MostrarEditar , cargarDatos: cargaDatos ,  proveedorEdit: proveedorEdit }} />





        </>
    );
}

export default usuarios;