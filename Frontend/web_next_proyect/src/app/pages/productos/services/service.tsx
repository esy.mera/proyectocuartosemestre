import useObtener from "@/app/hooks/obtener"

const enlistar= async(empresa:number, pagina:number)=>{
    let datosDefinitivos=[];
    
    let datos= await useObtener(`producto/PorEmpresa/${empresa}/?pagina=${pagina}`);
    let entregaData= datos.data
    for (let i = 0; i < entregaData.length; i++) {
        let objeto:datosProductos={
            id:0,
            nombre:"",
            descripcion:"",
            stock:0,
            precio:0.0,
            cantidad:0,
            proveedor:"",
            categoria:"",
            imagen:"",
            tipoImagen:"",
            idImagen:0,
            nombreImg:"",
            categoriaId:0,
            proveedorId:0
        }
        
        objeto.id=entregaData[i][0]
        objeto.nombre=entregaData[i][1]
        objeto.descripcion=entregaData[i][2]
        objeto.stock=entregaData[i][3]
        objeto.precio=entregaData[i][4]
        objeto.cantidad=entregaData[i][5]
        objeto.proveedor=entregaData[i][6]
        objeto.categoria=entregaData[i][7]
        objeto.idImagen=entregaData[i][8]
        objeto.categoriaId=entregaData[i][9]
        objeto.proveedorId=entregaData[i][10]
        let imagen= await useObtener(`images/${entregaData[i][8]}/`);
        objeto.imagen=imagen.data['datos']
        objeto.tipoImagen=imagen.data['tipo']
        objeto.nombreImg=imagen.data["nombre"]

        
        console.log(imagen)
        datosDefinitivos.push(objeto)
        
    }
    return datosDefinitivos

    
}
export default enlistar;