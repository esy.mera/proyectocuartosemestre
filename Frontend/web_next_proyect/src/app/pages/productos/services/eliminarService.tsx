import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";

let eliminarServ=async (id:any, idImagen:any)=>{
    const token = Cookies.get("token")|| "";
    await useEliminar(`producto/${id}/`,token)
    await useEliminar(`images/${idImagen}/`, token)

}

export default eliminarServ;