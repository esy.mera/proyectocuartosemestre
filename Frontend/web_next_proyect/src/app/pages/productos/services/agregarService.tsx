
import agregarImgHook from "@/app/hooks/agregarImagen";

let agregarImg=async (body:any)=>{
    
    let datos= await agregarImgHook(body)
    
    return datos
}

export default agregarImg;