import { useForm } from "react-hook-form";
import agregarImg from "../services/agregarService";
import agregarproducto from "../services/agregarServic";
import { useEffect, useState } from "react";
import categorias from "../../functions/categorias";
import proveedores from "../../functions/proveedores";

const agregar = (props: any) => {
    const [categoriaData,setCategoriaData]=useState<any[]>([])
    const [proveedoresData,setProveedoresData]=useState<proveedorModel[]>([])

    let carga=async ()=>{
        let data= await categorias()
        setCategoriaData(data.data);
        let dataProveedor= await proveedores();
        setProveedoresData(dataProveedor)
    }
    useEffect(()=>{
        carga()
    },[])
    console.log(proveedoresData)

    const { miRef, agregar, cargarDatos} = props.datos;

    const { register, handleSubmit, reset } = useForm();

    const onSubmit = async (data: any) => {
        

        const formData = new FormData();
        formData.append('file', data.imagen[0]);


        let imagenSubida=await agregarImg(formData)
        let producto={
            cantidad:Number(data.cantidad) ,
            descripcion:data.descripcion,
            nombre:data.nombre,
            precioUnitario:Number(data.precioUnitario) ,
            stockMinimo:Number(data.stock) ,
            proveedores:{
                idProveedor:Number(data.proveedores)
            },
            idCategoria:{
                id:Number(data.categoria)
            },
            image:{
                id: imagenSubida.id
            }
        }
        console.log(producto)

        await agregarproducto(producto)
        await cargarDatos()
        cancelar()
    };


    
    const cancelar = () => {
        agregar()
        reset()

    }

    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">
                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Agregar Producto</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input {...register('nombre')} type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input {...register('descripcion')} type="text" placeholder="Descripcion" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input {...register('precioUnitario')} type="number" step="any" placeholder="Precio Unitario" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input {...register('stock')} type="number" placeholder="Stock" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input {...register('cantidad')} type="number" placeholder="Cantidad" className="input input-bordered bg-white text-black" />
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" {...register('categoria')} defaultValue={""}>
                        <option value="" disabled>Categorias</option>
                        {
                            categoriaData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombre}</option>
                                )
                            })
                        }
                    </select>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} {...register('proveedores')}>
                    <option value="" disabled>Proveedores</option>
                    {
                            proveedoresData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id_proveedor}>{data.nombre}</option>
                                )
                            })
                        }

                    </select>
                    <div className="form-control mb-2">
                        <div className="form-control w-full max-w-xs">
                            <label className="label">
                                <span className="label-text">Imagen del producto</span>
                            </label>
                            <input type="file" {...register('imagen')} className="file-input file-input-bordered w-full max-w-xs" />
                        </div>
                    </div>

                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default agregar