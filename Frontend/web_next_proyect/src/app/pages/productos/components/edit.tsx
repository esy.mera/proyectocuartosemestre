
import { useForm } from "react-hook-form";
import agregarImg from "../services/agregarService";
import agregarproducto from "../services/agregarServic";
import { useEffect, useState } from "react";
import useEliminar from "@/app/hooks/eliminar";
import Cookies from "js-cookie";
import categorias from "../../functions/categorias";
import proveedores from "../../functions/proveedores";

const editarform = (props: any) => {
    const [categoriaData,setCategoriaData]=useState<any[]>([])
    const [proveedoresData,setProveedoresData]=useState<proveedorModel[]>([])

    let carga=async ()=>{
        let data= await categorias()
        setCategoriaData(data.data);
        let dataProveedor= await proveedores();
        setProveedoresData(dataProveedor)
    }
    useEffect(()=>{
        carga()
    },[])

    const { miRef, editar, cargarDatos, productoEdit } = props.datos;

    const [nombre, setNombre] = useState<any>()
    const [descripcion, setDescripcion] = useState<any>()
    const [precioU, setPrecioU] = useState<any>()
    const [stock, setStock] = useState<any>()
    const [cantidad, setCantidad] = useState<any>()
    const [categoria, setCategoria] = useState<any>()
    const [proveedor, setProveedor] = useState<any>()

    const {register, handleSubmit, reset } = useForm();

    useEffect(() => {
        setNombre(productoEdit.nombre)
        setDescripcion(productoEdit.descripcion)
        setPrecioU(productoEdit.precio)
        setStock(productoEdit.stock)
        setCantidad(productoEdit.cantidad)
        setCategoria(productoEdit.categoriaId)
        setProveedor(productoEdit.proveedorId)

    }, [productoEdit])

    const onSubmit = async (data: any) => {
        let img;

        if(data.imagen[0]==undefined){
            img=productoEdit.idImagen
            console.log(img)
        }else{
            console.log("si hay imagen")
            
            const formData = new FormData();
        formData.append('file', data.imagen[0]);
        console.log(data.imagen[0])

        let imagenSubida=await agregarImg(formData)
            img=imagenSubida.id
        }

        
        let producto = {
            id: productoEdit.id,
            cantidad: Number(cantidad),
            descripcion: descripcion,
            nombre: nombre,
            precioUnitario: Number(precioU),
            stockMinimo: Number(stock),
            proveedores: {
                idProveedor: Number(proveedor)
            },
            idCategoria: {
                id: Number(categoria)
            },
            image: {
                id: img //imagenSubida.id
            }
        }
        console.log(producto)

        await agregarproducto(producto)

        if(data.imagen[0]==undefined){
            console.log("no se elimina imagen")
        }else{

            const token = Cookies.get("token")|| "";
            await useEliminar(`images/${productoEdit.idImagen}/`, token)
            console.log("imagen antigua eliminada")
        }
        await cargarDatos()
        cancelar()
        reset()
    };



    const cancelar = () => {
        editar()
        reset()

    }

    return (
        <div ref={miRef} className="h-screen w-full absolute ancho justify-center items-center overflow-hidden z-50 desactivado">
            <div className="card card-edit flex-shrink-0 w-full max-w-sm shadow-2xl card-color-flotante">
                <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center text-4xl text-white mb-6">
                        <h1>Editar Producto</h1>
                    </div>
                    <div className="form-control mb-2">
                        <input value={nombre} onChange={(e) => {
                            const campo = e.target.value;
                            setNombre(campo)
                        }} type="text" placeholder="Nombre" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input value={descripcion} onChange={(e) => {
                            const campo = e.target.value;
                            setDescripcion(campo)
                        }} type="text" placeholder="Descripcion" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input value={precioU} onChange={(e) => {
                            const campo = e.target.value;
                            setPrecioU(campo)
                        }} type="number" step="any" placeholder="Precio Unitario" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input value={stock} onChange={(e) => {
                            const campo = e.target.value;
                            setStock(campo)
                        }} type="number" placeholder="Stock" className="input input-bordered bg-white text-black" />
                    </div>
                    <div className="form-control mb-2">
                        <input value={cantidad} onChange={(e) => {
                            const campo = e.target.value;
                            setCantidad(campo)
                        }} type="number" placeholder="Cantidad" className="input input-bordered bg-white text-black" />
                    </div>
                    <select className="select bg-white w-full max-w-xs text-black" value={categoria} onChange={(e) => {
                        const campo = e.target.value;
                        setCategoria(campo)
                    }} defaultValue={""}>
                        <option value="" disabled>Categorias</option>
                        {
                            categoriaData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id}>{data.nombre}</option>
                                )
                            })
                        }
                    </select>
                    <select className="select bg-white w-full max-w-xs text-black" defaultValue={""} value={proveedor} onChange={(e) => {
                        const campo = e.target.value;
                        setProveedor(campo)
                    }}>
                        <option value="" disabled>Proveedores</option>
                        {
                            proveedoresData.map((data, index)=>{
                                return(
                                    <option key={index} value={data.id_proveedor}>{data.nombre}</option>
                                )
                            })
                        }

                    </select>
                    <div className="form-control mb-2">
                        <div className="form-control w-full max-w-xs">
                            <label className="label">
                                <span className="label-text">Imagen del producto</span>
                            </label>
                            <input type="file" {...register('imagen')} className="file-input file-input-bordered w-full max-w-xs" />
                            <label className="label">

                                <span className="label-text-alt"></span>
                                <span className="label-text-alt">{productoEdit.nombreImg}</span>
                            </label>
                        </div>
                    </div>

                    <div className="form-control mt-6 flex btn-group-horizontal justify-between">
                        <button className="btn text-black bg-red-700 hover:bg-red-600 boton-bien" type="button" onClick={cancelar}>Cancelar</button>
                        <button type="submit" className="btn boton-color text-black boton-bien">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}


export default editarform 