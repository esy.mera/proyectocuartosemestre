"use client";

import imagenLogo from "@/app/assets/logo_pricipal.png"
import editar from "@/app/assets/edit.svg"
import eliminar from "@/app/assets/eliminar.svg"
import mas from "@/app/assets/mas.svg"
import Image from 'next/image'
import React, { useEffect, useRef, useState } from 'react'
import Agregar from "../components/agregar";
import Editarform from "../components/edit";
import { MostrarVentanas} from "../../functions/GlobalFunctions";
import Navbar from "../../GlobalComponents/navbar";
import { useRouter } from 'next/navigation' //router
import enlistar from "../services/service";
import eliminarServ from "../services/eliminarService";


function usuarios(param: any) {
    let router=useRouter()
    const [loading, setLoading] = useState(true);
    const [datos, setDatos] = useState<datosProductos[]>([]);
    const [datosComprobar, setDatosComprobar] = useState<datosProductos[]>([]);

    let parametroId = Number(param.params.id);
    const editarPantalla = useRef<HTMLDivElement>(null);
    const AgregarPantalla = useRef<HTMLDivElement>(null);

    const [productoEdit, setProductoEdit] = useState<Object>({})

    

    let MostrarAgregar = () => {
        MostrarVentanas(AgregarPantalla);
    };

    let MostrarEditar = () => {
        MostrarVentanas(editarPantalla);
    };

    let cargaDatos = async () => {
        let empresa = Number(localStorage.getItem("empresa"));
        const datosRecuperados = await enlistar(empresa, parametroId);
        setDatos(datosRecuperados);
        setLoading(false);
        
    };

    useEffect(() => {
        if(parametroId<=0){
            router.push(`/pages/productos/1`)
        }
        cargaDatos();
        
    }, []);


    return (
        <><div className="absolute bg w-full h-screen overflow-y-scroll">


            <Navbar />


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo" />
                <div className="text-center text-4xl text-white">Gestionar Productos</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="overflow-x-auto text-white bg-white">
                    <table className="table text-black ocultar">
                        {/* head */}
                        <thead className=" text-white">
                            <tr className="bg-gray-700 text-center">
                                <th>Nombre</th>
                                <th>Precio Unitario</th>
                                
                                <th>Categoria</th>
                                <th>Proveedores</th>
                                <th></th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <th>
                                    <button className="btn btn-ghost hover:bg-blue-600" onClick={MostrarAgregar}><Image
                                        src={mas}
                                        width={23}
                                        alt="logo" /></button>
                                </th>
                            </tr>
                            

                            {datos.map((producto,index) => {
                                return (
                                    <tr key={index}>
                                        <td>
                                            <div className="flex items-center space-x-3">
                                                <div className="avatar">
                                                    <div className="mask mask-squircle w-12 h-12">
                                                        <Image
                                                            src={`data:${producto.tipoImagen};base64,${producto.imagen}`}
                                                            width={100}
                                                            height={100}
                                                            alt="logo" />
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="font-bold">{producto.nombre}</div>
                                                    <div className="text-sm">{producto.descripcion}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {producto.precio}
                                        </td>
                                        
                                        <td>{producto.categoria}</td>
                                        <td>{producto.proveedor}</td>
                                        <td>
                                        <div>
                                                    <div className="font-bold">Cantidad:{producto.cantidad}</div>
                                                    <div className="text-sm">Minimo:{producto.stock}</div>
                                                </div>
                                        </td>
                                        <th className="flex justify-center ">
                                            <button className="btn btn-ghost hover:bg-yellow-400" onClick={()=>{
                                                    setProductoEdit({
                                                        id:producto.id,
                                                        nombre:producto.nombre,
                                                        descripcion:producto.descripcion,
                                                        stock:producto.stock,
                                                        precio:producto.precio,
                                                        cantidad:producto.cantidad,
                                                        proveedorId:producto.proveedorId,
                                                        categoriaId:producto.categoriaId,
                                                        idImagen:producto.idImagen,
                                                        nombreImg:producto.nombreImg
                                                    })
                                                    MostrarEditar()

                                                }}><Image
                                                src={editar}
                                                alt="logo" /></button>
                                        </th>
                                        <th>
                                            <button className="btn btn-ghost hover:bg-red-600" ><Image
                                                src={eliminar}
                                                alt="logo" onClick={async ()=>{
                                                    await eliminarServ(producto.id, producto.idImagen);
                                                    cargaDatos()
                                                }} /></button>
                                        </th>
                                    </tr>
                                )
                            })}



                        </tbody>

                    </table>

                </div>

            </div>
            <div className="flex justify-center pb-20">
                <div className="join bg">
                    <button className="join-item btn" onClick={()=>{
                            router.push(`/pages/productos/${parametroId-1}`)

                        
                    }}>«</button>
                    <button className="join-item btn">Pagina {parametroId}</button>
                    <button className="join-item btn" onClick={()=>{
                        
                            router.push(`/pages/productos/${parametroId+1}`)

                        
                    }}>»</button>
                </div>
            </div>

        </div>

            <Agregar datos={{ miRef: AgregarPantalla, agregar: MostrarAgregar, cargarDatos: cargaDatos }} />
            <Editarform datos={{ miRef: editarPantalla, editar: MostrarEditar, cargarDatos: cargaDatos, productoEdit:productoEdit }} />





        </>
    );
}

export default usuarios;