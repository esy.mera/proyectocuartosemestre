"use client"

import imagenLogo from "@/app/assets/logo_pricipal.png"
import imagenPersona from "@/app/assets/persona.png"
import Image from 'next/image'
import Navbar from "../pages/GlobalComponents/navbar"
import usuario from "@/app/assets/person-circle.svg"
import proveedores from "@/app/assets/people-fill.svg"
import almacen from "@/app/assets/shop-window.svg"
import categorias from "@/app/assets/journal-text.svg"
import productos from "@/app/assets/cart4.svg"
import { useRouter } from 'next/navigation' //router


export default function () {

    const ruta=useRouter()

    return (
        <div className="min-h-screen bg">
            <Navbar/>


            <div className="flex flex-col items-center mt-6">
                <Image
                    src={imagenLogo}
                    width={230}
                    alt="logo"
                />
                <div className="text-center text-6xl text-white">BIENVENIDOS</div>
            </div>

            <div className="flex flex-wrap mt-20 pb-10 justify-center gap-7">
                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={imagenPersona}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de roles </h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none" onClick={()=>{
                                ruta.push("/pages/roles/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>

                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={usuario}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de Usuarios</h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none" onClick={()=>{
                                ruta.push("/pages/usuarios/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>

                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={productos}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de Productos</h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none" onClick={()=>{
                                ruta.push("/pages/productos/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>

                

                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={categorias}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de Categorias</h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none0"onClick={()=>{
                                ruta.push("/pages/categorias/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>

                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={proveedores}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de Proveedores</h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none" onClick={()=>{
                                ruta.push("/pages/proveedores/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>

                <div className="card card-compact card-color shadow-xl ms-5">
                    <figure className="bg-white">
                        <Image
                            src={almacen}
                            width={230}
                            alt="logo"
                        />
                    </figure>
                    <div className="card-body flex flex-col justify-between card-sizes">
                        <h2 className="card-title text-black">Gestión de Almacen</h2>

                        <div className="card-actions justify-center">
                            <button className="btn boton-color text-black border-none" onClick={()=>{
                                ruta.push("/pages/almacenes/1")
                            }}>Acceder</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    );
}